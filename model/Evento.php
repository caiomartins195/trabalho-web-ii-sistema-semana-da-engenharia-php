<?php

require_once ("../inc/db.class.php");

class Evento {

    private $nome_evento;
    private $publico_alvo;
    private $responsaveis;
    private $local;
    private $tipo;
    private $qtd_vagas;
    private $data_evento;
    private $hora_evento;
    private $descricao_evento;
    private $pre_requisitos;
    private $imagem;
    private $status; //1 - Ativado, 0 - Desativado

    public function __construct($nome_evento, $publico_alvo, $responsaveis, $local, $tipo, $qtd_vagas, $data_evento, $hora_evento, $descricao_evento, $pre_requisitos, $imagem) {
        $this->nome_evento      = $nome_evento;
        $this->publico_alvo     = $publico_alvo;
        $this->responsaveis     = $responsaveis;
        $this->local            = $local;
        $this->tipo             = $tipo;
        $this->qtd_vagas        = $qtd_vagas;
        $this->data_evento      = $data_evento;
        $this->hora_evento      = $hora_evento;
        $this->descricao_evento = $descricao_evento;
        $this->pre_requisitos   = $pre_requisitos;  
        $this->imagem           = $imagem;
        $this->status           = 1; //O evento vem ativado por default
    }

    public function insertEvent($connection, $nome_evento){

        $evento_existe = false;

        //verifica na base se o administrador ja existe
        $sql = "select * from eventos where nomeEvento = '$nome_evento'";
        if($result = mysqli_query($connection, $sql)){
            $dados_evento = mysqli_fetch_array($result);
            if (isset($dados_evento))
                $evento_existe = true;
        }

        //inserção de usuario no BD
        $sql = "insert into eventos (nomeEvento, publicoAlvo, responsaveis, local, tipo, quantidadeVagas, dataEvento, horaEvento, descricao, requisitos, imagem, status) 
                values ('$this->nome_evento', '$this->publico_alvo', '$this->responsaveis', '$this->local', '$this->tipo', '$this->qtd_vagas', '$this->data_evento', '$this->hora_evento', '$this->descricao_evento', '$this->pre_requisitos', '$this->imagem', '$this->status')";

        //executar query
        if(!$evento_existe){
            mysqli_query($connection, $sql);
        }

        return $evento_existe;
    }

}