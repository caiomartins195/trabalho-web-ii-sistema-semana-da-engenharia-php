<?php

require_once ("../inc/db.class.php");

class Administrador {

    private $senha;
    private $matricula;
    private $nome;
    private $email;
    private $cargo;

    public function __construct($senha, $matricula, $nome, $email) {
        $this->nome        = $nome;
        $this->email       = $email;
        $this->matricula   = $matricula;
        $this->senha       = $senha;
        $this->cargo       = 'admin';
    }

    public function insertAdmin($connection, $matricula){

        $usuario_existe = false;

        //verifica na base se o administrador ja existe
        $sql = "select * from usuarios where matricula = '$matricula' and cargo = 'admin'";
        if($result = mysqli_query($connection, $sql)){
            $dados_usuario = mysqli_fetch_array($result);
            if (isset($dados_usuario))
                $usuario_existe = true;
        }

        //inserção de usuario no BD
        $sql = "insert into usuarios (senha, matricula, nome, email, cargo) 
                values ('$this->senha', '$this->matricula', '$this->nome', '$this->email', '$this->cargo')";

        //executar query
        if(!$usuario_existe){
            mysqli_query($connection, $sql);
        }

        return $usuario_existe;
    }

}