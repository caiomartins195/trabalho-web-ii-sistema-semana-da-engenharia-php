<?php

require_once ("../inc/db.class.php");

class Usuario {

    private $senha;
    private $matricula;
    private $nome;
    private $email;
    private $curso;
    private $periodo;
    private $semestre;
    private $cargo;

    public function __construct($senha, $matricula, $nome, $email, $curso, $periodo, $semestre) {
        $this->senha     = $senha;
        $this->matricula = $matricula;
        $this->nome      = $nome;
        $this->email     = $email;
        $this->curso     = $curso;
        $this->periodo   = $periodo;
        $this->semestre  = $semestre;
        $this->cargo     = 'user';
    }
        
    public function insertUser($connection, $matricula){
        
        $usuario_existe = false;

        //verifica na base se o usuaio ja existe
        $sql = "select * from usuarios where matricula = '$matricula' and cargo = 'user'";

        if($result = mysqli_query($connection, $sql)){
            $dados_usuario = mysqli_fetch_array($result);
            if (isset($dados_usuario))
                $usuario_existe = true;   
        }

        //inserção de usuario no BD
        $sql = "insert into usuarios (senha, matricula, nome, email, curso, periodo, semestre, cargo) 
                values ('$this->senha', '$this->matricula', '$this->nome', '$this->email', '$this->curso', '$this->periodo', '$this->semestre', '$this->cargo')";

        //executar query
        if(!$usuario_existe){
            mysqli_query($connection, $sql);
        }

        return $usuario_existe;

    }
    

}