<?php

require_once ("db.class.php");
require_once ("..\model\admin.php");

// conexão com a base de dados
$objDb = new db();
$link = $objDb->conecta_mysql();

$id_evento = $_GET['evento_id'];
$qtd_vagas = $_POST['quantidade_de_vagas'];
$foto = $_FILES['foto'];

$status_att = 0;

if(isset($qtd_vagas)){
    $sql = "UPDATE eventos SET quantidadeVagas = {$qtd_vagas} WHERE ideventos = {$id_evento}";
    $result = mysqli_query($link, $sql);
    $status_att = 1;
}

if($foto['error'] == 0){
    // Pega extensão da imagem
    preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
    // Gera um nome único para a imagem
    $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
    // Caminho de onde ficará a imagem
    $caminho_imagem = "../upload/";
    move_uploaded_file($foto["tmp_name"], $caminho_imagem.$nome_imagem);
    $sql = "UPDATE eventos SET imagem = '{$nome_imagem}' WHERE ideventos = {$id_evento}";
    $result = mysqli_query($link, $sql);
    $status_att = 1;
}

?>

<form id='return' method='get' action='../admin/admin_editar_evento.php'>
    <input type="hidden" value="<?php echo $id_evento?>" name="id_evento">
    <input type="hidden" value="<?php echo $status_att?>" name="status_att">
</form>
<script language='JavaScript'>
    document.forms['return'].submit();;
</script>

