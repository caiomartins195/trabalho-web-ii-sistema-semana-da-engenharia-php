<?php

require_once ("db.class.php");
require_once ("..\model\usuario.php");

// conexão com a base de dados
$objDb = new db();
$link = $objDb->conecta_mysql();

$matricula = $_POST['matricula'];
$senha = $_POST['senha'];

// traz os dados do usuario ao logar
$sql = "select * from usuarios where matricula = '{$matricula}' AND senha = '{$senha}'";
$result = mysqli_query($link, $sql);
$dados_usuario = mysqli_fetch_assoc($result);

$logged_user = new usuario($dados_usuario["senha"], $dados_usuario["matricula"], $dados_usuario["nome"], $dados_usuario["email"], 
                           $dados_usuario["curso"], $dados_usuario["periodo"], $dados_usuario["semestre"]);

// caso o usuario não exista na base de dados, este é redirecionado para a tela de login com uma mensagem de erro
if (!isset($dados_usuario)){
    ?>
        <form id='return' method='get' action='../index.php'>
        <input type="hidden" value="1" name="usuario_nao_cadastrado">
        </form>
        <script language='JavaScript'>
            document.forms['return'].submit();
        </script>
    <?php
} else if($dados_usuario['cargo'] == "user"){
    ?>
        <form id='return' method='post' action='../user/user_page.php'>
            <input type="hidden" value="<?php echo $dados_usuario["id"] ?>" name="id">
            <input type="hidden" value="<?php echo $dados_usuario["senha"] ?>" name="senha">
            <input type="hidden" value="<?php echo $dados_usuario["matricula"] ?>" name="matricula">
            <input type="hidden" value="<?php echo $dados_usuario["nome"] ?>" name="nome">
            <input type="hidden" value="<?php echo $dados_usuario["email"] ?>" name="email">
            <input type="hidden" value="<?php echo $dados_usuario["curso"] ?>" name="curso">
            <input type="hidden" value="<?php echo $dados_usuario["periodo"] ?>" name="periodo">
            <input type="hidden" value="<?php echo $dados_usuario["semestre"] ?>" name="semestre">
        </form>
        <script language='JavaScript'>
            document.forms['return'].submit();
        </script>
    <?php
} else if($dados_usuario['cargo'] == "admin") {
    ?>
        <form id='return' method='post' action='../admin/admin_page.php'>
            <input type="hidden" value="<?php echo $dados_usuario["id"] ?>" name="id">
            <input type="hidden" value="<?php echo $dados_usuario["senha"] ?>" name="senha">
            <input type="hidden" value="<?php echo $dados_usuario["matricula"] ?>" name="matricula">
            <input type="hidden" value="<?php echo $dados_usuario["nome"] ?>" name="nome">
            <input type="hidden" value="<?php echo $dados_usuario["email"] ?>" name="email">
        </form>
        <script language='JavaScript'>
            document.forms['return'].submit();
        </script>
    <?php
}

?>