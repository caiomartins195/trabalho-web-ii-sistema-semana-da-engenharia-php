<?php

require_once ("db.class.php");
// conexão com a base de dados
$objDb = new db();
$link = $objDb->conecta_mysql();

$id_evento = $_POST['id_evento'];

$sql = "SELECT ideventos, quantidadeVagas FROM eventos WHERE eventos.ideventos = {$id_evento}";
$result = mysqli_query($link, $sql);
$qtd_vagas_eventos = mysqli_fetch_assoc($result);

$sql = "SELECT COUNT(*) AS qtd FROM inscricoes WHERE inscricoes.evento_id = {$qtd_vagas_eventos['ideventos']}";
$result = mysqli_query($link, $sql);
$qtd_inscritos_evento = mysqli_fetch_assoc($result);

if ($qtd_vagas_eventos['quantidadeVagas'] == $qtd_inscritos_evento['qtd']) {
    echo 1;
} else {
    echo 0;
}

?>