<?php

require_once ("db.class.php");
require_once ("..\model\Evento.php");

// conexão com a base de dados
$objDb = new db();
$link = $objDb->conecta_mysql();

$foto = $_FILES["foto"];
$nome_evento = $_POST["nome_do_evento"];
// Pega extensão da imagem
preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto["name"], $ext);
// Gera um nome único para a imagem
$nome_imagem = md5(uniqid(time())) . "." . $ext[1];
// Caminho de onde ficará a imagem
$caminho_imagem = "../upload/";

$new_event = new evento($_POST["nome_do_evento"], $_POST["publico_alvo"], $_POST["responsaveis"], $_POST["local_Evento"], $_POST["tipo_do_evento"], 
                        $_POST["quantidade_de_vagas"], $_POST["dataEvento"], $_POST["horaEvento"], $_POST["descricao_evento"], 
                        $_POST["pre_requisitos"], $nome_imagem);

$evento_existe = $new_event->insertEvent($link, $_POST["nome_do_evento"]);   

if($evento_existe == ''){
    // Faz o upload da imagem para seu respectivo caminho
    move_uploaded_file($foto["tmp_name"], $caminho_imagem.$nome_imagem);
    $evento_existe = 0; //evento não existe
} else {
    $evento_existe = 1; //evento existe
}

?>

<form id='return' method='get' action='../admin/admin_cadastrar_evento.php'>
<input type="hidden" value="<?php echo $evento_existe?>" name="evento_existe">
</form>
<script language='JavaScript'>
    document.forms['return'].submit();;
</script>