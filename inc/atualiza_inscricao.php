<?php

    require_once ("db.class.php");
    $objDb = new db();
    $link = $objDb->conecta_mysql();

    $id_usuario = $_POST['id_usuario'];
    $id_evento = $_POST['id_evento'];

    $sql = "SELECT COUNT(*) AS inscricao FROM inscricoes WHERE inscricoes.evento_id = {$id_evento} AND inscricoes.usuario_id = {$id_usuario}";
    $result = mysqli_query($link, $sql);
    $status_inscricao = mysqli_fetch_assoc($result);

    $sql = "SELECT dataEvento, horaEvento FROM eventos WHERE eventos.ideventos = {$id_evento}";
    $result = mysqli_query($link, $sql);
    $array = mysqli_fetch_assoc($result);
    $data_evento = $array['dataEvento'];
    $hora_evento = $array['horaEvento'];
    
    if ($status_inscricao["inscricao"] == 1) {
        $sql = "DELETE FROM inscricoes WHERE inscricoes.evento_id = {$id_evento} AND inscricoes.usuario_id = {$id_usuario}";
        $result = mysqli_query($link, $sql);
    } else if ($status_inscricao["inscricao"] == 0) {
        $sql = "INSERT INTO inscricoes (usuario_id,evento_id,dataEvento,horaEvento) VALUES ({$id_usuario}, {$id_evento}, '{$data_evento}', '{$hora_evento}')";
        $result = mysqli_query($link, $sql);
    }

    $sql = "SELECT nome FROM usuarios, inscricoes, eventos 
    WHERE eventos.ideventos = " . $id_evento . "
    AND inscricoes.usuario_id = usuarios.id
    AND inscricoes.evento_id = eventos.ideventos;";
    $r = mysqli_query($link, $sql);
    $arr = array();
    while($nomes = mysqli_fetch_assoc($r)){
        array_push($arr, $nomes['nome']);
    }
    echo json_encode($arr);

?>