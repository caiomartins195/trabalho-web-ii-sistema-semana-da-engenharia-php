<?php

	class db {

		//host
		private $host = 'localhost';

		//usuario
		private $usuario = 'root';

		//senha
		private $senha = '';

		//database
		private $database = 'semana_engenharia';

		public function conecta_mysql ()
		{
			//conexao
			$con = mysqli_connect($this->host, $this->usuario, $this->senha, $this->database);

			mysqli_set_charset($con, 'utf8');

			//verificacao de erros
			if (mysqli_connect_errno()) {
				echo "Erro ao tentar se conectar ao banco de dados MySQL".mysqli_connect_error();
			}

			return $con;
		}
	}

?>