<?php

    require_once ("db.class.php");
    // conexão com a base de dados
    $objDb = new db();
    $link = $objDb->conecta_mysql();

    $id_evento = $_POST['id'];

    $sql = "SELECT status FROM eventos WHERE ideventos = {$id_evento}";
    $result = mysqli_query($link, $sql);
    $status_evento = mysqli_fetch_assoc($result);

    if($status_evento['status'] == 1){
        $sql = "UPDATE eventos SET status = 0 WHERE ideventos = {$id_evento}";
        $result = mysqli_query($link, $sql);
        echo "Status do evento atualizado";
    } else if ($status_evento['status'] == 0){
        $sql = "UPDATE eventos SET status = 1 WHERE ideventos = {$id_evento}";
        $result = mysqli_query($link, $sql);
        echo "Status do evento atualizado";
    } else {
        echo "Houve um erro no sistema";
    }
?>