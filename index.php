<?php

session_start();
session_destroy();

?>

<!DOCTYPE html>
<html>
<head lang="pt-br">
	<title>Semana da Engenharia UNIFAE</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" type="image/x-icon" href="img/icon.png">
	<link rel="stylesheet" type="text/css" href="css/3-col-portfolio.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link href="css/template.css" rel="stylesheet">
	<link href="css/index.css" rel="stylesheet">
</head>
<body>
	<section>
		<!-- Conteudo geral -->
		<div class="container geral">
			<div class="alert alert-danger" id="usuario_existe" role="alert">
				<?php
					// falha no cadastro
					if(isset($_GET["usuario_existe"]) && $_GET["usuario_existe"] == 1){
						echo'<style>
							#usuario_existe {
								display: block;
								text-align: center;
								font-weight: bold;
							}
						</style>';
						echo "Usuário já cadastrado!";
					} else {
						echo'<style>
							#usuario_existe {
								display: none;
							}
						</style>';
					}
				?>
			</div>
			<div class="alert alert-danger" id="alert" role="alert">
			<?php
				// falha no login
				if(isset($_GET["usuario_nao_cadastrado"]) && $_GET["usuario_nao_cadastrado"] ==  1){
					echo'<style>
						#alert {
							display: block;
							text-align: center;
							font-weight: bold;
						}
					</style>';
					echo "Usuário não cadastrado!";
				} else {
					echo'<style>
						#alert {
							display: none;
						}
					</style>';
				}
			?>
			</div>
			<div class="alert alert-success" id="success" role="alert">
			<?php
				if(isset($_GET["usuario_existe"]) && $_GET["usuario_existe"] == 0){
					echo'<style>
						#success {
							display: block;
							text-align: center;
							font-weight: bold;
						}
					</style>';
					echo "Usuário cadastrado com sucesso!";
				} else {
					echo'<style>
						#success {
							display: none;
						}
					</style>';
				}
			?>
			</div>
			<div class="row">
				<div class="col-xl-10 col-lg-8 col-md-8 col-sm-8 col-10 credentials">
					<img class="logo" src="img/logoNavbar.png" alt="logo semana da engenharia" title="logo da semana da engenharia">
				</div>
			</div>
			<div class="row">
				<div class="col-xl-4 col-lg-5 col-md-6 col-sm-6 col-8 credentials">
					<form class="form-horizontal" method="post" action="inc/valida_login.php">
						<div class="row">
							<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
								<div class="form-group">
									<label for="matricula">Matricula:</label>
									<input type="text" class="form-control form-login" id="matricula" name="matricula" aria-describedby="emailHelp" placeholder="Insira sua matricula aqui" required>
									<small class="legenda">Número de registro na instituição. Deve conter 5 números, hífen e o digito</small>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
								<div class="form-group">
									<label for="senha">Senha:</label>
									<input type="password" class="form-control form-login" id="senha" name="senha" placeholder="Insira sua senha aqui" required>
									<small class="legenda">Deve possuir no mínimo 4 caracteres, possuindo pelo menos 1 letra e 1 número</small>
								</div>
							</div>	
						</div>
						<div class="row">
							<div align="center" class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 custom_btn">
								<button class="btn btn-success" id="login">Entrar</button>
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#user_register" id="register">Cadastre-se</button>		
							</div>	
						</div>
					</form>
				</div>
			</div>
		</div>

		<!-- Modal -->
		<div class="container">
			<div class="row modal fade" id="user_register" tabindex="-1" role="dialog"  aria-hidden="true">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="modal-dialog modal-lg">
						<div class="modal-content custom_modal">
							<div class="alert alert-danger" id="modal_alert" role="alert"></div>
							<div class="row">
								<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
									<h3 class="h3 title">Preencha o formulário abaixo e realize seu cadastro!</h3>		
								</div>
							</div>
							<div class="row">
								<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
									<form class="form-horizontal credentials" method="post" action="inc/cadastro_usuarios.php">
										<div class="row">
											<div class="form-group col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
												<label for="ra">Matrícula (R.A.):</label>
												<input type="text" class="form-control form-cadastro" id="matricula_cadastro" name="matricula" placeholder="Matricula (R.A.)" required>
												<small class="legenda">Número de registro na instituição. Deve conter 5 números, hífen e o digito</small>
											</div>
											<div class="form-group col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
												<label for="senha">Senha:</label>
												<input type="password" class="form-control form-cadastro" id="senha_cadastro" name="senha" placeholder="Senha" required>
												<small class="legenda">Deve possuir no mínimo 4 caracteres, possuindo pelo menos 1 letra e 1 número</small>
											</div>
											<div class="form-group col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
												<label for="senha">Confirmação de senha:</label>
												<input type="password" class="form-control form-cadastro" id="confirmacao_senha_cadastro" name="confirmacao_de_senha" placeholder="Confirmação" required>
												<small class="legenda">Informe sua senha novamente</small>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
												<label for="nome">Nome Completo:</label>
												<input type="text" class="form-control form-cadastro" id="nome" name="nome" placeholder="Insira seu nome completo aqui" required>
												<small class="legenda">Não é permitido o uso de números ou caracteres especiais</small>
											</div>
											<div class="form-group col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
												<label for="email">E-mail:</label>
												<input type="email" class="form-control form-cadastro" id="email" name="email" placeholder="Insira seu e-mail aqui" required>
												<small class="legenda">E-mail para possíveis contatos</small>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
												<label for="curso">Curso:</label>
												<input type="text" class="form-control form-cadastro" id="curso" name="curso" placeholder="Insira seu curso aqui" required>
												<small class="legenda">Curso realizado na instituição</small>
											</div>
											<div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-6 col-6">
												<label for="periodo">Período:</label>
												<select class="form-control form-cadastro" id="periodo" name="periodo" required>
													<option selected></option>
													<option value="diurno">Diurno</option>
													<option value="vespertino">Vespertino</option>
													<option value="noturno">Noturno</option>
												</select>
												<small class="legenda">Período em que o curso é realizado</small>
											</div>
											<div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-6 col-6">
												<label for="semestre">Semestre:</label>
												<select class="form-control form-cadastro" id="semestre" name="semestre" required>
													<option selected></option>
													<option value="1">1°</option>
													<option value="2">2°</option>
													<option value="3">3°</option>
													<option value="4">4°</option>
													<option value="5">5°</option>
													<option value="6">6°</option>
													<option value="7">7°</option>
													<option value="8">8°</option>
													<option value="9">9°</option>
													<option value="10">10°</option>
												</select>
												<small class="legenda">Semestre que esta sendo cursado</small>
											</div>
										</div>
										<div class="custom_btn row">
											<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" align="center">
												<button class="btn btn-primary" id="register_new_user">Cadastrar</button>
												<button class="btn btn-danger" data-dismiss="modal">Fechar</button>
											</div>
										</div>
									</form>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Fim Modal -->
	</div>
	<!-- /. Conteudo geral -->
</section>

<!-- Bootstrap core JavaScript -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>

<!-- Custom Js -->
<script src="js/index.js"></script>
</body>
</html>