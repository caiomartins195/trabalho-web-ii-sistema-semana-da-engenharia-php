-- MySQL dump 10.16  Distrib 10.1.26-MariaDB, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: semana_engenharia
-- ------------------------------------------------------
-- Server version	10.1.26-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `eventos`
--

DROP TABLE IF EXISTS `eventos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eventos` (
  `ideventos` int(11) NOT NULL AUTO_INCREMENT,
  `nomeEvento` varchar(45) NOT NULL,
  `publicoAlvo` varchar(45) NOT NULL,
  `responsaveis` varchar(45) NOT NULL,
  `local` varchar(45) NOT NULL,
  `tipo` varchar(45) NOT NULL,
  `quantidadeVagas` int(11) NOT NULL,
  `dataEvento` date NOT NULL,
  `horaEvento` time NOT NULL,
  `descricao` varchar(100) NOT NULL,
  `requisitos` varchar(45) NOT NULL,
  `imagem` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`ideventos`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eventos`
--

LOCK TABLES `eventos` WRITE;
/*!40000 ALTER TABLE `eventos` DISABLE KEYS */;
INSERT INTO `eventos` VALUES (1,'Curso de PHP ','Engenharia de Software','Caio Martins','Lab 1','Mini Curso',0,'2018-11-15','19:30:00','Silvio Santos Ipsum mah ooooee vem pra c. Vem pra c. Um, dois trs, quatro, PIM, entendeuam? Ha haeee','HTML','aa55de6c02ec46bd50584e0c34a8b311.png',0),(2,'Curso de Drupal','Engenharia de Software','Karim Maluf','Lab 3','Mini Curso',10,'2018-11-20','19:30:00','Noções básicas sobre o framework PHP - Drupal','PHP','f1306e1e3c214c4e968864a586ef0274.png',1),(3,'Metodologia Agil','Engenharia de Software','Claudete Moscardini','Quadra UNIFAE','Mini Curso',50,'2018-11-14','19:30:00','Treinamento de metodologia agil pela doutura Claudete','Curso de UML','90073b88349f299db8c0336020674c55.png',1),(6,'Curso de Angular','Engenharia de Software','Dornellas','CETEP','Mini Curso',100,'2018-11-15','19:30:00','Curso sobre o framework javascript Angular','JavaScript','36844cdc990272e55d10f11664a1db96.png',1);
/*!40000 ALTER TABLE `eventos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inscricoes`
--

DROP TABLE IF EXISTS `inscricoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inscricoes` (
  `usuario_id` int(11) NOT NULL,
  `evento_id` varchar(45) NOT NULL,
  `dataEvento` date DEFAULT NULL,
  `horaEvento` time DEFAULT NULL,
  PRIMARY KEY (`usuario_id`,`evento_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inscricoes`
--

LOCK TABLES `inscricoes` WRITE;
/*!40000 ALTER TABLE `inscricoes` DISABLE KEYS */;
INSERT INTO `inscricoes` VALUES (4,'6','2018-11-16','19:30:00');
/*!40000 ALTER TABLE `inscricoes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `senha` varchar(45) NOT NULL,
  `matricula` varchar(45) NOT NULL,
  `nome` varchar(45) NOT NULL,
  `email` varchar(150) NOT NULL,
  `curso` varchar(45) DEFAULT NULL,
  `periodo` varchar(45) DEFAULT NULL,
  `semestre` varchar(45) DEFAULT NULL,
  `cargo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'admin123','00001-1','Cristiane Imamura','',NULL,NULL,NULL,'admin'),(4,'caio123','22890-8','Caio Martins','caiomartins.cm@gmail.com','Engenharia de Software','noturno','6','user'),(5,'123asd','23400-5','Gustavo Franco','guf@email.com','Engenharia de Software','noturno','6','user');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-15 11:50:40
