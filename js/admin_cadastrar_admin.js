var input_names;


/*-- Função que controla o aparecimento da caixa de alerta ao usuario: esta deve receber um elemento, 
o seu estado (se esta correto o preenchimento do campo ou não), 
e um alert que indica qual caixa deverá mudar de estado (alert ou modal_alert) --*/
function alert_personalizado(campo, estado, alert){
	if (!estado) {
		$("#"+alert).text("Preencha o(s) campo(s) " + campo + " corretamente").css({"text-align":"center", "font-weight":"bold"});
		$("#"+alert).css({"display": "block"});
	} else {
		$("#"+alert).css({"display": "none"});
	}
}

/*-- Função que garante que não sejam submetidos inputs com valor em branco e captura 
seus nomes para alerta --*/
function valida_inputs(input_names, alert) {
	var id;
	var string = "";
	var aux = true;

	$(input_names).each(function(index, el) {
		id = el.id;
		if ($("#"+id).val() == "") {
			$("#"+id).css({"border": "2px solid red"});
			string += " " + el.name;
			aux = false;
		} else {
			$("#"+id).css({"border": "2px solid green"});
		}
	});

	alert_personalizado(string, aux, alert);
	return aux;
}

jQuery(document).ready(function($) {

	/*-- Validando as informações de Cadastro para não haverem campos vazios --*/
	$("#cadastrar").click(function() {
		input_names = new Array();

		$(".form-control, .form-control-file").each(function(index, el) {
			input_names.push(el);
		});
		
		var aux = valida_inputs(input_names, "alert");
		return aux;      
	});

	$("#nomeAdmin").change(function(event) {		
		str = $("#nomeAdmin").val();
		regexp = new RegExp(/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ'\s]+$/);
		if((!regexp.test(str))) {
			$("#nomeAdmin").val("");
			$("#nomeAdmin").focus();
		}
		valida_inputs($("#nomeAdmin"), "alert");
	});

	/*-- Validando o campo matricula: este só poderá conter 6 numeros e um hifen --*/
	$("#matricula").change(function(event) {
		str = $("#matricula").val();
		regexp = new RegExp(/^[0-9]{5}-[0-9]{1}/); // essa regex foi feita na raça
		if((!regexp.test(str))) {
			$("#matricula").val("");
			$("#matricula").focus();
		}
		valida_inputs($("#matricula"), "alert");
	});
	
	$("#email").change(function(event) {		
		str = $("#email").val();
		regexp = new RegExp(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/gi);
		if((!regexp.test(str))) {
			$("#email").val("");
			$("#email").focus();
		}
		valida_inputs($("#email"), "alert");
	});

	$("#senhaAdministrador").change(function(event) {
		str = $("#senhaAdministrador").val();
		regexp = new RegExp(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,}$/);
		if(!regexp.test(str)) {
			$("#senhaAdministrador").val("");
			$("#senhaAdministrador").focus();matricula
		}
		valida_inputs($("#senhaAdministrador"), "alert");
	});

	$("#confirmaSenha").change(function(event) {
		if ($("#senhaAdministrador").val() !== $("#confirmaSenha").val()) {
			$("#alert").text("As senhas digitadas não conferem, por favor, verifique sua senha").css({"text-align":"center", "font-weight":"bold"});
			$("#alert").css({"display": "block"});
			$("#confirmaSenha").val("").css({"border": "2px solid red"}).focus();
		} else {
			$("#alert").css({"display": "none"});
			$("#confirmaSenha").css({"border": "2px solid green"});
		}
	});

	$("#cancelar").click(function() {
		$(location).attr('href', 'admin_page.php');
	})
});