/*Função para mudar css ao desativar o evento*/
function addClassByClick(id) {
  if ($("#" + id).hasClass("desativado")) {
    /*Mudar estado*/
    $("#" + id).removeClass("desativado");
    $("#modal" + id).removeClass("desativado");
    /*Mudar botão*/
    $(".toggleButton").filter("#modalButton" + id).removeClass("fa-toggle-off");
    $(".toggleButton").filter("#modalButton" + id).addClass("fa-toggle-on");
    /*Mudar tooltip*/
    $("#tooltipText" + id).tooltip("dispose");
    $("#tooltipText" + id).attr({ "title": "Desativar evento" }).tooltip();
    /*Mudar texto modal*/
    $("#modalText" + id).val("");
    $("#modalText" + id).text(" Desativar evento");

  } else {
    $("#" + id).addClass("desativado");
    $("#modal" + id).addClass("desativado");

    $(".toggleButton").filter("#modalButton" + id).removeClass("fa-toggle-on");
    $(".toggleButton").filter("#modalButton" + id).addClass("fa-toggle-off");

    $("#tooltipText" + id).tooltip("dispose");
    $("#tooltipText" + id).attr({ "title": "Ativar evento" }).tooltip();


    $("#modalText" + id).val("");
    $("#modalText" + id).text(" Ativar evento");
  }
}