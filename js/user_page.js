function block_ao_logar(id_usuario) {
	$.ajax({
		method: "post",
		dataType: "json",
		data: { 'id_usuario': id_usuario },
		url: "../inc/dados_eventos_inscritos.php",
		success: function (data) {
			for (var i = 0; i < data.length; i++) {
				blockInscricaoEvento(data[i][0]);
			}
		},
	});
}

// desativa eventos desabilitados por administradores do sistema
function desativar_evento(id) {
	$("#" + id).addClass("desativado");
	$("#modal" + id).addClass("desativado");
	$("#btn_inscrever-se" + (id)).prop("disabled", true);
	$("#btn_inscrever-se_modal_" + (id)).prop("disabled", true);
	if ($("#modalButton" + id).hasClass("fa-check")) { //Detecta se é um botão de inscrição
		$("#btn_inscrever-se" + (id)).tooltip("dispose");
		$("#btn_inscrever-se" + (id)).attr({ "title": "Evento desativado!" }).tooltip();
	} else {
		$("#btn_inscrever-se" + (id)).tooltip("dispose");
		$("#btn_inscrever-se" + (id)).attr({ "title": "Evento desativado, sua inscrição foi cancelada, pedimos desculpas pelo transtorno!" }).tooltip();
	}

}

// verifica a data atual do sistema, e consulta na base de dados a data do evento atual, caso sejam iguais, o evento é bloqueado
function diaDeEventoBloquear(id_evento) {
	var dataAtual = new Date();
	var stringData = dataAtual.getFullYear() + "-" + (dataAtual.getMonth() + 1) + "-" + dataAtual.getDate(); //pega a data atual
	$.ajax({
		method: "post",
		dataType: "json",
		data: { 'id_evento': id_evento },
		url: "../inc/get_datas_eventos.php",
		success: function (data) {
			var dataEvento = new Date(data['dataEvento']);
			console.log(dataAtual.getDate(), (dataEvento.getDate() + 1));
			if (!$("#" + (id_evento)).hasClass("desativado")) {
				if (stringData == data['dataEvento']) {
					//Desativa o botão
					$("#btn_inscrever-se" + id_evento).prop("disabled", true);
					$("#btn_inscrever-se_modal_" + id_evento).prop("disabled", true);
					$("#btn_inscrever-se" + id_evento).tooltip("dispose");
					$("#btn_inscrever-se" + id_evento).attr({ "title": "O evento é hoje! Inscrições e desinscrições bloqueadas!" }).tooltip();
				} else if (dataAtual.getDate() > (dataEvento.getDate() + 1)) {
					$("#btn_inscrever-se" + id_evento).prop("disabled", true);
					$("#btn_inscrever-se_modal_" + id_evento).prop("disabled", true);
					$("#btn_inscrever-se" + id_evento).tooltip("dispose");
					$("#btn_inscrever-se" + id_evento).attr({ "title": "O evento já ocorreu!" }).tooltip();
				}
			}
		},
	});
}

// bloqueia eventos que estão com as vagas esgotadas -- menos para usuarios ja inscritos neste
function lotacao(id_evento) {
	var aux; // caso o evento não seja bloqueado por lotação, o sistema irá verificar a data 
	$.ajax({
		method: "post",
		data: { 'id_evento': id_evento },
		url: "../inc/lotacao_eventos.php",
		success: function (data) {
			if (data == 1) {
				if (!$("#" + (id_evento)).hasClass("desativado")) {
					if ($("#modalButton" + id_evento).hasClass("fa-check")) { //Detecta se é um botão de inscrição
						//Desativa o botão
						$("#btn_inscrever-se" + id_evento).prop("disabled", true);
						$("#btn_inscrever-se_modal_" + id_evento).prop("disabled", true);
						$("#btn_inscrever-se" + id_evento).tooltip("dispose");
						$("#btn_inscrever-se" + id_evento).attr({ "title": "Evento Lotado!" }).tooltip();
						var aux = false;
					}
				}
			}
		}
	});
}

// desbloqueia o evento de mesma data quando o usuário se desinscrever deste
function unblockEvento(id_evento) {
	var data_evento;
	$.ajax({
		method: "get",
		dataType: "json",
		url: "../inc/dados_eventos.php",
		success: function (data) {
			var dados_eventos = [];
			for (var index = 0; index < data.length; index++) {
				dados_eventos[index] = [];
				dados_eventos[index]['id'] = data[index][0];
				dados_eventos[index]['dataEvento'] = data[index][1];
				dados_eventos[index]['horaEvento'] = data[index][2];
			}
			for (var index = 0; index < dados_eventos.length; index++) {
				if (dados_eventos[index]['id'] == id_evento) {
					data_evento = dados_eventos[index]['dataEvento'];
				}
			}
			for (var index = 0; index < dados_eventos.length; index++) {
				if (dados_eventos[index]['dataEvento'] == data_evento) {
					if (!$("#" + dados_eventos[index]['id']).hasClass("desativado")) {
						if ($("#modalButton" + dados_eventos[index]['id']).hasClass("fa-check")) { //Detecta se é um botão de inscrição
							//Desativa o botão
							$("#btn_inscrever-se" + dados_eventos[index]['id']).prop("disabled", false);
							$("#btn_inscrever-se_modal_" + dados_eventos[index]['id']).prop("disabled", false);
							$("#btn_inscrever-se" + dados_eventos[index]['id']).tooltip("dispose");
							$("#btn_inscrever-se" + dados_eventos[index]['id']).attr({ "title": "Inscrever-se" }).tooltip();
							lotacao(dados_eventos[index]['id']);
						}
					}
				}
			}
		}
	});
}

// função que bloqueia um evento de mesma data que o evento que o usuario se inscreveu
function blockInscricaoEvento(id_evento) {
	var data_evento;
	$.ajax({
		method: "get",
		dataType: "json",
		url: "../inc/dados_eventos.php",
		success: function (data) {
			var dados_eventos = [];
			// traz os dados relevantes para bloqueio de outros eventos que tenham a mesma data que o evento que o usuario se inscreveu
			for (var index = 0; index < data.length; index++) {
				dados_eventos[index] = [];
				dados_eventos[index]['id'] = data[index][0];
				dados_eventos[index]['dataEvento'] = data[index][1];
				dados_eventos[index]['horaEvento'] = data[index][2];
			}
			// todas as datas de eventos são trazidas acima, aqui definimos qual é a data do evento que o usuario se inscreveu
			for (var index = 0; index < dados_eventos.length; index++) {
				if (dados_eventos[index]['id'] == id_evento) {
					data_evento = dados_eventos[index]['dataEvento'];
				}
			}
			//tendo a data do evento em mãos, percorremos todas as datas de eventos, e bloqueamos aqueles que possuem a mesma data
			for (var index = 0; index < dados_eventos.length; index++) {
				if (dados_eventos[index]['dataEvento'] == data_evento) {
					if (!$("#" + dados_eventos[index]['id']).hasClass("desativado")) { // um evento desativado não deve sofrer essa verificação
						if ($("#modalButton" + dados_eventos[index]['id']).hasClass("fa-check")) {//Detecta se é um botão de inscrição
							//Desativa o botão
							$("#btn_inscrever-se" + dados_eventos[index]['id']).prop("disabled", true);
							$("#btn_inscrever-se_modal_" + dados_eventos[index]['id']).prop("disabled", true);
							$("#btn_inscrever-se" + dados_eventos[index]['id']).tooltip("dispose");
							$("#btn_inscrever-se" + dados_eventos[index]['id']).attr({ "title": "Você já se cadastrou em um evento neste periodo!" }).tooltip();
							lotacao(dados_eventos[index]['id']);
						}
					}


				}
			}
		}
	});
}

// troca os icones de inscricão e desinscrição
function addClassByClick(id) {
	if (!$("#" + (id)).hasClass("desativado")) {
		if ($("#modalButton" + id).hasClass("fa-check")) {//Caso não esteja inscrito no evento
			$("#modalButton" + id).removeClass("fa-check");
			$(".toggleButton").filter("#modalButton" + id).removeClass("fa-check");
			$("#btn_inscrever-se" + id).children(".inscreva-se").val("");
			$("#btn_inscrever-se_modal_" + id).children(".inscreva-se").val("");
			$("#btn_inscrever-se" + id).children(".inscreva-se").text(" Desinscrever-se");
			$("#btn_inscrever-se_modal_" + id).children(".inscreva-se").text(" Desinscrever-se");
			$("#btn_inscrever-se" + id).tooltip("dispose");
			$("#btn_inscrever-se" + id).attr({ "title": "Desinscrever-se" }).tooltip();
			$(".toggleButton").filter("#modalButton" + id).addClass("fa-check-square");
			blockInscricaoEvento(id);
		} else {
			$("#modalButton" + id).addClass("fa-check");
			$(".toggleButton").filter("#modalButton" + id).removeClass("fa-check-square");
			$("#btn_inscrever-se" + id).children(".inscreva-se").val("");
			$("#btn_inscrever-se_modal_" + id).children(".inscreva-se").val("");
			$("#btn_inscrever-se" + id).children(".inscreva-se").text(" Inscrever-se");
			$("#btn_inscrever-se_modal_" + id).children(".inscreva-se").text(" Inscrever-se");
			$("#btn_inscrever-se" + id).tooltip("dispose");
			$("#btn_inscrever-se" + id).attr({ "title": "Inscrever-se" }).tooltip();
			$(".toggleButton").filter("#modalButton" + id).addClass("fa-check");
			unblockEvento(id);
		}
	}
}

// ações realizadas ao se inscrever ou se desinscrever de um evento
function acoes(id_evento, id_usuario) {
	addClassByClick(id_evento);
	atualiza_inscricao(id_usuario, id_evento);
}