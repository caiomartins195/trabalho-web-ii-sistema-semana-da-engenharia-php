function atualiza_inscricao(id_usuario, id_evento) {
    var aux = true;
    var numero_de_inscricoes = 0;

    $.ajax({
        method: "post",
        dataType: "json",
        url: "../inc/atualiza_inscricao.php",
        data: { 'id_usuario': id_usuario, 'id_evento': id_evento },
        success: function (data) {
            var items = [];
            $.each(data, function (key, val) {
                items.push("<li>" + val + "</li>");
            });
            $("#lista_inscritos" + id_evento).html(items);
            $("#qtdInscritos" + id_evento).html(items.length);
            $("#modal_qtdInscritos" + id_evento).html(items.length);
            if ($("#modalButton" + id_evento).hasClass("fa-check")) {
                numero_de_inscricoes = +$("#numero_de_inscricoes").text();
                numero_de_inscricoes--;
                $("#numero_de_inscricoes").html(numero_de_inscricoes);
                alert("Desinscrição feita com sucesso");
            }
            if ($("#modalButton" + id_evento).hasClass("fa-check-square")) {
                numero_de_inscricoes = +$("#numero_de_inscricoes").text();
                numero_de_inscricoes++;
                $("#numero_de_inscricoes").html(numero_de_inscricoes);
                alert("Inscrição feita com sucesso");
            }
        }
    });
}

function muda_status(id) {
    $.ajax({
        method: "post",
        url: "../inc/muda_status.php",
        data: { 'id': id },
        success: function (data) {
            alert(data);
        }
    });
}