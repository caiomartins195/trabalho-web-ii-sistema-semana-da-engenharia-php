var input_names;
var usuarios_cadastrados;

/*-- Função que controla o aparecimento da caixa de alerta ao usuario: esta deve receber um elemento, 
o seu estado (se esta correto o preenchimento do campo ou não), 
e um alert que indica qual caixa deverá mudar de estado (alert ou modal_alert) --*/
function alert_personalizado(campo, estado, alert){
	if (!estado) {
		$("#"+alert).text("Preencha o(s) campo(s) " + campo + " corretamente").css({"text-align":"center", "font-weight":"bold"});
		$("#"+alert).css({"display": "block"});
	} else {
		$("#"+alert).css({"display": "none"});
	}
}

/*-- Função que garante que não sejam submetidos inputs com valor em branco e captura 
seus nomes para alerta --*/
function valida_inputs_vazios(input_names, alert) {
	var id;
	var string = "";
	var aux = true;

	$(input_names).each(function(index, el) {
		id = el.id;
		if ($("#"+id).val() == "") {
			$("#"+id).css({"border": "2px solid red"});
			string += " " + el.name;
			aux = false;
		} else {
			$("#"+id).css({"border": "2px solid green"});
		}
	});

	alert_personalizado(string, aux, alert);
	return aux;
}

jQuery(document).ready(function($) {
	var aux;

	setTimeout(function () {
		$('.logo').hide().fadeIn( 4000 );
		$(".form-horizontal").hide().fadeIn( 4000 );
	});

	/*-- Validando o campo matricula: este só poderá conter 6 numeros e um hifen --*/
	$("#matricula").change(function(event) {
		str = $("#matricula").val();
		regexp = new RegExp(/^[0-9]{5}-[0-9]{1}/); // essa regex foi feita na raça
		if((!regexp.test(str))) {
			$("#matricula").val("");
			$("#matricula").focus();
		}

		valida_inputs_vazios($("#matricula"), "alert");
	});

	/*-- Validando o campo senha: este deve possuir no minimo 4 caracteres, possuindo pelo menos 1 letra e 1 numero --*/
	$("#senha").change(function(event) {
		str = $("#senha").val();
		regexp = new RegExp(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,}$/);
		if(!regexp.test(str)) {
			$("#senha").val("");
			$("#senha").focus();matricula
		}

		valida_inputs_vazios($("#senha"), "alert");
	});

	/*-- Caso o usuario pressione a tecla enter no botão de login, sera direcionado para a função de click --*/
	$("#login").keypress(function() {
		if(e.which == 13) $('#login').click();
	});

	$("#register_new_user").keypress(function() {
		if(e.which == 13) $('#register_new_user').click();
	});


	$("#login").click(function() {
		input_names = new Array();
		$(".form-login").each(function(index, el) {
			input_names.push(el);
		});
		aux = valida_inputs_vazios(input_names, "alert");

		return aux;

	});

	$("#register_new_user").click(function() {
		input_names = new Array();
		aux = true;
		var new_user = new Object();
		$(".form-cadastro").each(function(index, el) {
			input_names.push(el);
		});
		aux = valida_inputs_vazios(input_names, "modal_alert");
		
		if (aux) {
			$(location).attr('href', 'index.php');
		} else {
			return aux;
		}

	});

	/*-- Regras de Negócios --*/
	var str; // conteudo dos inputs
	var regexp; // expressão regular

	/*-- Validando o campo matricula: este só poderá conter 6 numeros e um hifen --*/
	$("#matricula_cadastro").change(function(event) {
		str = $("#matricula_cadastro").val();
		regexp = new RegExp(/^[0-9]{5}-[0-9]{1}/); // essa regex foi feita na raça
		if((!regexp.test(str))) {
			$("#matricula_cadastro").val("");
			$("#matricula_cadastro").focus();
		}
		valida_inputs_vazios($("#matricula_cadastro"), "modal_alert");
	});

	/*-- Validando o campo senha: este deve possuir no minimo 4 caracteres, possuindo pelo menos 1 letra e 1 numero --*/
	$("#senha_cadastro").change(function(event) {
		str = $("#senha_cadastro").val();
		regexp = new RegExp(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,}$/);
		if(!regexp.test(str)) {
			$("#senha_cadastro").val("");
			$("#senha_cadastro").focus();
		}
		valida_inputs_vazios($("#senha_cadastro"), "modal_alert");
	});

	/*-- Validando o campo de confirmação de senha: este deve possuir exatamente o mesmo conteúdo que o campo senha --*/
	$("#confirmacao_senha_cadastro").change(function(event) {
		if ($("#confirmacao_senha_cadastro").val() !== $("#senha_cadastro").val()) {
			$("#modal_alert").text("As senhas digitadas não conferem, por favor, verifique sua senha").css({"text-align":"center", "font-weight":"bold"});
			$("#modal_alert").css({"display": "block"});
			$("#confirmacao_senha_cadastro").val("").css({"border": "2px solid red"}).focus();
		} else {
			$("#modal_alert").css({"display": "none"});
			$("#confirmacao_senha_cadastro").css({"border": "2px solid green"});
		}
	});

	/*-- Validando o campo nome: este só poderá conter letras e espaços, e apóstrofes se necessário --*/
	$("#nome").change(function(event) {		
		str = $("#nome").val();
		regexp = new RegExp(/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ'\s]+$/);
		if((!regexp.test(str)) || (str.length < 4)) {
			$("#nome").val("");
			$("#nome").focus();
		}
		valida_inputs_vazios($("#nome"), "modal_alert");
	});

	/*-- Validando o campo email: este deverá possuir obrigatoriamente um '@' e um '.' após --*/
	$("#email").change(function(event) {		
		str = $("#email").val();
		regexp = new RegExp(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/gi);
		if((!regexp.test(str))) {
			$("#email").val("");
			$("#email").focus();
		}
		valida_inputs_vazios($("#email"), "modal_alert");
	});

	/*-- Validando o campo curso: este só poderá conter letras e espaços --*/
	$("#curso").change(function(event) {		
		str = $("#curso").val();
		regexp = new RegExp(/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ\s]+$/);
		if((!regexp.test(str))) {
			$("#curso").val("");
			$("#curso").focus();
		}
		valida_inputs_vazios($("#curso"), "modal_alert");
	});

	$("#periodo").change(function(event) {
		valida_inputs_vazios($("#periodo"), "modal_alert");
	});
	

	$("#semestre").change(function(event) {
		valida_inputs_vazios($("#semestre"), "modal_alert");
	});
});