// var input_names;

// /*-- Função que controla o aparecimento da caixa de alerta ao usuario: esta deve receber um elemento, 
// o seu estado (se esta correto o preenchimento do campo ou não), 
// e um alert que indica qual caixa deverá mudar de estado (alert ou modal_alert) --*/
// function alert_personalizado(campo, estado, alert){
// 	if (!estado) {
// 		$("#"+alert).text("Preencha o(s) campo(s) " + campo + " corretamente").css({"text-align":"center", "font-weight":"bold"});
// 		$("#"+alert).css({"display": "block"});
// 	} else {
// 		$("#"+alert).css({"display": "none"});
// 	}
// }

// /*-- Função que garante que não sejam submetidos inputs com valor em branco e captura 
// seus nomes para alerta --*/
// function valida_inputs(input_names, alert) {
// 	var id;
// 	var string = "";
// 	var aux = true;

// 	$(input_names).each(function(index, el) {
// 		id = el.id;
// 		if ($("#"+id).val() == "") {
// 			$("#"+id).css({"border": "2px solid red"});
// 			string += " " + el.name;
// 			aux = false;
// 		} else {
// 			$("#"+id).css({"border": "2px solid green"});
// 		}
// 	});

// 	alert_personalizado(string, aux, alert);

// 	return aux;
// }

// jQuery(document).ready(function($) {

// 	/*-- Validando as informações de Cadastro para não haverem campos vazios --*/
// 	$("#alterar").click(function() {
// 		input_names = new Array();

// 		$("#quantVagas").each(function(index, el) {
// 			input_names.push(el);
// 		});
// 		$(".form-control-file").each(function(index, el) {
// 			input_names.push(el);
// 		})
		
// 		var aux = valida_inputs(input_names, "alert");
// 		return aux;      
// 	});

// 	$("#quantVagas").change(function(event) {		
// 		str = $("#quantVagas").val();
// 		regexp = new RegExp(/^[0-9]/);
// 		if((!regexp.test(str))) {
// 			$("#quantVagas").val("");
// 			$("#quantVagas").focus();
// 		}
// 		valida_inputs($("#quantVagas"), "alert");
// 	});

// });

$("#cancelar").click(function() {
    $(location).attr('href', 'admin_page.php');
})