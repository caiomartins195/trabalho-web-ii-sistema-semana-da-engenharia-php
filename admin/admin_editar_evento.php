<?php

require_once ("../inc/db.class.php");

$id_evento = $_GET['id_evento'];
if(isset($_GET['status_att']))
    $status_att = $_GET['status_att'];

// conexão com a base de dados
$objDb = new db();
$link = $objDb->conecta_mysql();

$sql = "SELECT * FROM eventos WHERE ideventos={$id_evento}";
$result = (mysqli_query($link, $sql));
$dados_evento = mysqli_fetch_assoc($result);

?>

<!DOCTYPE html>
<html>

<head lang="pt-br">
    <title>Semana da Engenharia UNIFAE</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/x-icon" href="../img/icon.png">
    <link rel="stylesheet" type="text/css" href="../css/3-col-portfolio.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/admin_cadastra_evento.css">
    <link href="../css/template.css" rel="stylesheet">
    <!-- Link para icones -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
        crossorigin="anonymous">

</head>

<body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bgColor fixed-top">
        <div class="container">
            <a class="navbar-brand" href="admin_page.php"><img src="../img/logoNavbar.png" height=25% width="150"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="admin_page.php">Home
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li>
                        <div class="dropdown">
                            <a href="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false" class="nav-link text-white">Cadastrar <i class="fas fa-caret-down"></i></a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="admin_cadastrar_evento.php">Evento</a>
                                <a class="dropdown-item" href="admin_cadastrar_admin.php">Administrador</a>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="../index.php">Logout
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Page Content -->
    <div class="container geral">
        <div class="alert alert-danger" id="alert" role="alert">
            <?php
				// falha na att
				if(isset($status_att) && ($status_att == 0)){
					echo'<style>
							#alert {
							display: block;
							text-align: center;
							font-weight: bold;
						}
					</style>';
					echo "Erro ao atualizar evento!";
				} else {
					echo'<style>
						#alert {
							display: none;
						}
					</style>';
				}
			?>
        </div>
        <div class="alert alert-success" id="success" role="alert">
            <?php
				// atualização ok
				if(isset($status_att) && ($status_att == 1)){
					echo'<style>
							#success {
							display: block;
							text-align: center;
							font-weight: bold;
						}
					</style>';
					echo "Evento atualizado com sucesso!";
				} else {
					echo'<style>
						#success {
							display: none;
						}
					</style>';
				}
			?>
        </div>
        <div class="row">
            <div id="intro" class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <h3 id="intro" align="center">Editar Evento</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <form class="form-horizontal" method="post" action="../inc/atualiza_evento.php?evento_id=<?php echo $id_evento?>" enctype="multipart/form-data">
                    <div class="row">
                        <div class="form-group col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <label for="nomeEvento">Nome do Evento:</label>
                            <input type="text" class="form-control" id="nomeEvento" name="nome_do_evento" value="<?php echo $dados_evento['nomeEvento']?>"
                                disabled="none">
                        </div>
                        <div class="form-group col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <label for="publicoAlvo">Público Alvo:</label>
                            <select class="form-control" id="publicoAlvo" name="publico_alvo" disabled="none">
                                <?php echo "<option selected>{$dados_evento['publicoAlvo']}</option>"?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <label for="executor">Responsaveis:</label>
                            <input type="text" class="form-control" id="executor" name="Responsaveis" value="<?php echo $dados_evento['responsaveis']?>"
                                disabled="none">
                        </div>
                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <label for="tipo">Tipo:</label>
                            <select class="form-control" id="tipo" name="tipo_do_evento" disabled="none">
                                <?php echo "<option selected>{$dados_evento['tipo']}</option>"?>
                            </select>
                        </div>
                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <label for="quantVagas">Quantidade de Vagas:</label>
                            <input type="text" class="form-control" id="quantVagas" name="quantidade_de_vagas" value="<?php echo $dados_evento['quantidadeVagas']?>">
                        </div>
                        <div class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                            <label for="dataEvento">Data do Evento</label>
                            <input type="text" class="form-control" name="dataEvento" id="dataEvento" disabled="none" value="<?php echo date('d/m/Y', strtotime($dados_evento["dataEvento"])) ?>">
                        </div>
                        <div class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                            <label for="horaEvento">Hora do Evento</label>
                            <input type="text" class="form-control" name="horaEvento" id="horaEvento" disabled="none" value="<?php echo date('H:i', strtotime($dados_evento["horaEvento"])) ?>">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <label for="descricao">Descrição do Evento:</label>
                            <input type="text" class="form-control" id="descricao" name="descricao_evento" value="<?php echo $dados_evento["descricao"] ?>" disabled="none"></textarea>
                        </div>
                        <div class="form-group col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <label for="requisitos">Pré Requisitos:</label>
                            <input type="text" class="form-control" id="requisitos" name="pre_requisitos" value="<?php echo $dados_evento["requisitos"] ?>" disabled="none"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <label for="inputFile">Selecione Uma Foto de Capa do Evento:</label>
                            <input type="file" id="input_file" class="form-control-file" id="foto" name="foto" accept="image/png, image/jpeg">
                        </div>
                    </div>

                    <div class="custom_btn row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" align="center">
                            <button class="btn btn-primary" id="alterar">Alterar</button>
                            <button type="button" class="btn btn-danger" id="cancelar">Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <!-- /.container -->
    <!-- Footer -->
    <footer class="page-footer font-small bgColor">
        <!-- Footer Elements -->
        <div class="container text-white py-5">
            <div class="row center">
                <div class="col-3">
                </div>
                <div class="col-2">
                    <a target="_blank" href="https://www.facebook.com/engenhariascompsoft/"><i class="fab fa-facebook-f footerIcon"></i></a>
                </div>
                <div class="col-2">
                    <a target="_blank" href="https://www.instagram.com/engsoftwareunifae/"><i class="fab fa-instagram footerIcon"></i></a>
                </div>
                <div class="col-2 ">
                    <a target="_blank" href="https://api.whatsapp.com/send?phone=5519992183875&text=Olá%20Anderson!"><i
                            class="fab fa-whatsapp footerIcon"></i></a>
                </div>
            </div>
        </div>
        <!-- Footer Elements -->
        <!-- Copyright -->
        <div class="footer-copyright text-center py-3 footerBgColor">
            © 2018 <b><span>UNI</span><span style="color: #FE2E2E">FAE</span> - Engenharia de Software</b>
        </div>
        <!-- Copyright -->
    </footer>
    <!-- Footer -->
    <!-- Bootstrap core JavaScript -->
    </script>
    </script>
    <script>
    </script>
    <script src="http://momentjs.com/downloads/moment.js"></script>
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/bootstrap.bundle.min.js"></script>
    <!-- Custom JavaScript -->
    <script src="../js/admin_editar_evento.js"></script>
</body>

</html>