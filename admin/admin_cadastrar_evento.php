<!DOCTYPE html>
<html>

<head lang="pt-br">
    <title>Semana da Engenharia UNIFAE</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/x-icon" href="../img/icon.png">
    <link rel="stylesheet" type="text/css" href="../css/3-col-portfolio.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/admin_cadastra_evento.css">
    <link href="../css/template.css" rel="stylesheet">
    <!-- Link para icones -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
        crossorigin="anonymous">
</head>

<body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bgColor fixed-top">
        <div class="container">
            <a class="navbar-brand" href="admin_page.php"><img src="../img/logoNavbar.png" height=25% width="150"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="admin_page.php">Home
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li>
                        <div class="dropdown">
                            <a href="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false" class="nav-link text-white">Cadastrar <i class="fas fa-caret-down"></i></a>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="admin_cadastrar_evento.php">Evento</a>
                                <a class="dropdown-item" href="admin_cadastrar_admin.php">Administrador</a>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="../index.php">Logout
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Page Content -->
    <div class="container geral">
        <div class="alert alert-danger" id="alert" role="alert">
            <?php
				// falha no cadastro
				if(isset($_GET["evento_existe"]) && $_GET["evento_existe"] == 1){
					echo'<style>
							#alert {
							display: block;
							text-align: center;
							font-weight: bold;
						}
					</style>';
					echo "Evento já cadastrado!";
				} else {
					echo'<style>
						#alert {
							display: none;
						}
					</style>';
					}
			?>
        </div>
        <div class="alert alert-success" id="success" role="alert">
            <?php
				// falha no cadastro
				if(isset($_GET["evento_existe"]) && $_GET["evento_existe"] == 0){
					echo'<style>
							#success {
							display: block;
							text-align: center;
							font-weight: bold;
						}
					</style>';
					echo "Evento cadastrado com sucesso!";
				} else {
					echo'<style>
						#success {
							display: none;
						}
					</style>';
					}
			?>
        </div>
        <div class="row">
            <div id="intro" class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <h3 id="intro" align="center">Cadastro de Eventos</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <form class="form-horizontal" method="post" action="../inc/cadastro_evento.php" enctype="multipart/form-data">
                    <div class="row">
                        <div class="form-group col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <label for="nomeEvento">Nome do Evento:</label>
                            <input type="text" class="form-control" id="nomeEvento" name="nome_do_evento" placeholder="Nome do Evento">
                        </div>
                        <div class="form-group col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <label for="localEvento">Local do Evento:</label>
                            <input type="text" class="form-control" id="localEvento" name="local_Evento" placeholder="Local do Evento">
                        </div>
                        <div class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
                            <label for="publicoAlvo">Público Alvo:</label>
                            <select class="form-control" id="publicoAlvo" name="publico_alvo">
                                <option value=""></option>
                                <option>Engenharia de Software</option>
                                <option>Engenharia de Computação</option>
                                <option>Engenharia Civil</option>
                                <option>Engenharia Mecânica</option>
                                <option>Engenharia Elétrica</option>
                                <option>Engenharia Química</option>
                                <option>Engenharia de Produção</option>
                            </select>
                        </div>
                        <div class="form-group col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6">
                            <label for="tipo">Tipo:</label>
                            <select class="form-control" id="tipo" name="tipo_do_evento">
                                <option value=""></option>
                                <option>Mini Curso</option>
                                <option>Palestra</option>
                            </select>
                        </div>
                        <div class="form-group col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <label for="responsaveis">Responsaveis:</label>
                            <input type="text" class="form-control" id="executor" name="responsaveis" placeholder="Responsaveis">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <label for="quantVagas">Quantidade de Vagas:</label>
                            <input type="text" class="form-control" id="quantVagas" name="quantidade_de_vagas"
                                placeholder="Quantidade de Vagas">
                        </div>
                        <div class="form-group col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                            <label for="dataEvento">Data do Evento</label>
                            <input type="date" class="form-control" name="dataEvento" id="dataEvento">
                        </div>
                        <div class="form-group col-xl-3 col-lg-3 col-md-3 col-sm-6 col-12">
                            <label for="horaEvento">Hora do Evento</label>
                            <input type="time" class="form-control" name="horaEvento" id="horaEvento">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <label for="descricao">Descrição do Evento:</label>
                            <textarea class="form-control" id="descricao" name="descricao_evento" rows="4" style="resize:none;"
                                placeholder="Escreva aqui uma breve descrição do evento"></textarea>
                        </div>
                        <div class="form-group col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <label for="requisitos">Pré Requisitos:</label>
                            <textarea class="form-control" id="requisitos" name="pre_requisitos" rows="4" style="resize:none"
                                placeholder="Informe os pré requisitos que o participante deve possuir"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <label for="inputFile">Selecione Uma Foto de Capa do Evento:</label>
                            <input type="file" id="input_file" class="form-control-file" name="foto" accept="image/png, image/jpeg">
                        </div>
                    </div>

                    <div class="custom_btn row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" align="center">
                            <button class="btn btn-primary" id="cadastrar">Cadastrar</button>
                            <button type="button" class="btn btn-danger" id="cancelar">Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <!-- /.container -->
    <!-- Footer -->
    <footer class="page-footer font-small bgColor">
        <!-- Footer Elements -->
        <div class="container text-white py-5">
            <div class="row center">
                <div class="col-3">
                </div>
                <div class="col-2">
                    <a target="_blank" href="https://www.facebook.com/engenhariascompsoft/"><i class="fab fa-facebook-f footerIcon"></i></a>
                </div>
                <div class="col-2">
                    <a target="_blank" href="https://www.instagram.com/engsoftwareunifae/"><i class="fab fa-instagram footerIcon"></i></a>
                </div>
                <div class="col-2 ">
                    <a target="_blank" href="https://api.whatsapp.com/send?phone=5519992183875&text=Olá%20Anderson!"><i
                            class="fab fa-whatsapp footerIcon"></i></a>
                </div>
            </div>
        </div>
        <!-- Footer Elements -->
        <!-- Copyright -->
        <div class="footer-copyright text-center py-3 footerBgColor">
            © 2018 <b><span>UNI</span><span style="color: #FE2E2E">FAE</span> - Engenharia de Software</b>
        </div>
        <!-- Copyright -->
    </footer>
    <!-- Footer -->
    <!-- Bootstrap core JavaScript -->
    </script>
    </script>
    <script>
    </script>
    <script src="http://momentjs.com/downloads/moment.js"></script>
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/bootstrap.bundle.min.js"></script>
    <!-- Custom JavaScript -->
    <script src="../js/admin_cadastrar_evento.js"></script>
</body>

</html>