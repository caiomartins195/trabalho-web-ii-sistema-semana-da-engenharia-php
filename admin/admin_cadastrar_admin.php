<!DOCTYPE html>
<html>
<head lang="pt-br">
	<title>Semana da Engenharia UNIFAE</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" type="image/x-icon" href="../img/icon.png">
	<link rel="stylesheet" type="text/css" href="../css/3-col-portfolio.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/admin_cadastra_evento.css">
	<link href="../css/template.css" rel="stylesheet">
	<!-- Link para icones -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
</head>
<body>
	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg navbar-dark bgColor fixed-top">
		<div class="container">
			<a class="navbar-brand" href="admin_page.php"><img src="../img/logoNavbar.png" height=25% width="150"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active">
						<a class="nav-link" href="admin_page.php">Home
							<span class="sr-only">(current)</span>
						</a>
					</li>
					<li>
						<div class="dropdown">
							<a href="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false" class="nav-link text-white">Cadastrar <i class="fas fa-caret-down"></i></a>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="admin_cadastrar_evento.php">Evento</a>
								<a class="dropdown-item" href="admin_cadastrar_admin.php">Administrador</a>
							</div>
						</div>
					</li>
					<li class="nav-item active">
						<a class="nav-link" href="../index.php">Logout
							<span class="sr-only">(current)</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- Page Content -->
	<div class="container geral">
		<div class="alert alert-danger" id="admin_existe" role="alert">
			<?php
				// falha no cadastro
				if(isset($_GET["admin_existe"]) && $_GET["admin_existe"] == 1){
					echo'<style>
						#admin_existe {
							display: block;
							text-align: center;
							font-weight: bold;
						}
					</style>';
					echo "Administrador já cadastrado!";
				} else {
					echo'<style>
							#admin_existe {
							display: none;
						}
					</style>';
				}
			?>
		</div>
		<div class="alert alert-success" id="success" role="alert">
			<?php
				if(isset($_GET["admin_existe"]) && $_GET["admin_existe"] == 0){
					echo'<style>
						#success {
							display: block;
							text-align: center;
							font-weight: bold;
						}
					</style>';
					echo "Administrador cadastrado com sucesso!";
				} else {
					echo'<style>
						#success {
							display: none;
						}
					</style>';
				}
			?>
		</div>
		<div class="row">
			<div id="intro" class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<h3 id="intro" align="center">Cadastro de Administrador</h3>
			</div> 
		</div>
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<form class="form-horizontal cadastrar_admin" method="post" action="../inc/cadastro_administradores.php">
					<div class="row">
						<div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<label for="nomeAdmin">Nome do Administrador:</label>
							<input type="text" class="form-control" id="nomeAdmin" name="nome_do_administrador" placeholder="Nome do Administrador">
						</div>
						<div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
							<label for="email">Email:</label>
							<input type="text" class="form-control" id="email" name="email" placeholder="Email do Administrador">
						</div>
					</div>

					<div class="row">
						<div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
							<label for="matricula">Matrícula:</label>
							<input type="text" name="matricula_do_administrador" class="form-control" id="matricula" placeholder="Matrícula do Administrador">
							<small class="legenda">Número da Matrícula. Deve conter 5 números, hífen e o digito</small>
						</div>
						<div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
							<label for="senhaAdministrador">Senha:</label>
							<input type="password" class="form-control" id="senhaAdministrador" name="senhaAdministrador" placeholder="Senha">
							<small class="legenda">A senha deve conter pelo menos 4 caracteres com pelo menos 1 letra e 1 número</small>
						</div>
						<div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12">
							<label for="confirmaSenha">Confirmar Senha:</label>
							<input type="password" class="form-control" id="confirmaSenha" name="confirmaSenha" placeholder="Confirmar Senha">
						</div>
					</div>

					<div class="custom_btn row">
						<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" align="center">
							<button class="btn btn-primary" id="cadastrar">Cadastrar</button>
							<button type="button" class="btn btn-danger" id="cancelar">Cancelar</button>
						</div>
					</div>
				</form>
			</div>
		</div>

	</div>
	<!-- /.container -->
	<!-- Footer -->
	<footer class="page-footer font-small bgColor">
		<!-- Footer Elements -->
		<div class="container text-white py-5">
			<div class="row center">
				<div class="col-3">
				</div>
				<div class="col-2">
					<a target="_blank" href="https://www.facebook.com/engenhariascompsoft/"><i class="fab fa-facebook-f footerIcon"></i></a>
				</div>
				<div class="col-2">
					<a target="_blank" href="https://www.instagram.com/engsoftwareunifae/"><i class="fab fa-instagram footerIcon"></i></a>
				</div>
				<div class="col-2 ">
					<a target="_blank" href="https://api.whatsapp.com/send?phone=5519992183875&text=Olá%20Anderson!"><i class="fab fa-whatsapp footerIcon"></i></a>
				</div>
			</div>
		</div>
		<!-- Footer Elements -->
		<!-- Copyright -->
		<div class="footer-copyright text-center py-3 footerBgColor">
			© 2018 <b><span>UNI</span><span style="color: #FE2E2E">FAE</span> - Engenharia de Software</b>
		</div>
		<!-- Copyright -->
	</footer>
	<!-- Footer -->
	<!-- Bootstrap core JavaScript -->
	<script src="../js/jquery-3.3.1.min.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/bootstrap.bundle.min.js"></script>
	<!-- Custom JavaScript -->
	<script src="../js/admin_cadastrar_admin.js"></script>
</body>
</html>