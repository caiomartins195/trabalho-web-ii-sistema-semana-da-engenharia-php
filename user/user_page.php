<?php

session_start();

if(isset($_POST['nome'])){
    $_SESSION['user'] = $_POST['nome'];
}

require_once ("../inc/db.class.php");

// conexão com a base de dados
$objDb = new db();
$link = $objDb->conecta_mysql();

$sql = "SELECT * FROM eventos";
$result = (mysqli_query($link, $sql));
$numero_de_eventos = mysqli_num_rows($result);

$sql = "SELECT count(*) AS qtd FROM inscricoes WHERE usuario_id = {$_POST['id']}";
$result = (mysqli_query($link, $sql));
$numero_de_inscricoes = mysqli_fetch_assoc($result);

$sql = "SELECT * FROM eventos";
$result = (mysqli_query($link, $sql));

$sql_inscricao = "SELECT * FROM inscricoes, usuarios, eventos
                WHERE inscricoes.usuario_id = {$_POST['id']}
                AND inscricoes.usuario_id = usuarios.id
                AND inscricoes.evento_id = eventos.ideventos";
$retorno = mysqli_query($link, $sql_inscricao);
$inscricoes = mysqli_num_rows($retorno);

?>

<!DOCTYPE html>
<html>

<head lang="pt-br">
    <title>Semana da Engenharia UNIFAE</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/x-icon" href="../img/icon.png">
    <link rel="stylesheet" type="text/css" href="../css/3-col-portfolio.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link href="../css/template.css" rel="stylesheet">
    <link href="../css/admin.css" rel="stylesheet">
    <!-- Custom js -->
    <script src="../js/servicos.js"></script>
    <script src="../js/user_page.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script src="../js/jquery-3.3.1.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/bootstrap.bundle.min.js"></script>
    <!-- Link para icones -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
        crossorigin="anonymous">
</head>

<body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bgColor fixed-top">
        <div class="container">
            <a class="navbar-brand" href="#"><img src="../img/logoNavbar.png" height=25% width="150"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="../index.php">Logout
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Page Content -->
    <div class="container geral">
        <!-- Page Heading -->
        <h1 class="my-4 text-primary">
            Bem vindo <?php echo $_SESSION['user'] ?>
        </h1>
        <div class="row">
            <div class="col-lg-12 col-sm-6 portfolio-item">
                <div class="card h-100">
                    <div class="card-body">
                        <h4 class="card-title">
                            Estatisticas:
                        </h4>
                        <p class="card-text">
                            Existem <span id="numero_eventos_ativos"> <?php echo $numero_de_eventos ?> </span> eventos ativos no momento.
                            Você esta inscrito em <span id="numero_de_inscricoes"><?php echo $numero_de_inscricoes['qtd']?></span> evento(s).
                        </p>
                    </div>
                </div>
            </div>
            <!--Eventos -->
            <?php 
                while($registro = mysqli_fetch_assoc($result)){
                    // verifica quantos inscritos o evento possui e mostra na interface
                    $sql = "SELECT * FROM inscricoes WHERE evento_id = {$registro['ideventos']}";
                    $r = mysqli_query($link, $sql);
                    $numero_de_inscricoes = mysqli_num_rows($r);

                    
            ?>
            <div class="col-lg-4 col-sm-6 portfolio-item">
                <div class="card h-100" id=<?php echo $registro['ideventos'] ?>>
                    <a href="#" data-toggle="modal" data-target=<?php echo "#evento" . $registro['ideventos'] . "Modal" ?>><img class="cardImgSize" src=<?php echo "../upload/" . $registro['imagem'] ?>></a>
                    <div class="card-body">
                        <h4 class="card-title">
                            <a href="#" data-toggle="modal" data-target=<?php echo "#evento" . $registro['ideventos'] . "Modal" ?>><?php echo $registro["nomeEvento"] ?></a>
                        </h4>
                        <p class="card-text limit"><?php echo $registro["descricao"] ?></p>
                        <small>Por <?php echo $registro["responsaveis"] ?></small><br>
                        <small class="text-right">Local: <?php echo $registro["local"] ?></small><br>
                        <small class="vagas">Vagas ocupadas:
                            <span id="qtdInscritos<?php echo $registro['ideventos']?>"><?php echo $numero_de_inscricoes ?></span>/<span id="qtdDisponivel<?php echo $registro['ideventos']?>"><?php echo $registro["quantidadeVagas"] ?></span>
                        </small><br>
                        <small class="data">Data do evento:
                            <span id="diaEvento<?php echo $registro['ideventos']?>"><?php echo date('d', strtotime($registro["dataEvento"])) ?></span>/<span id="mesEvento<?php echo $registro['ideventos']?>"><?php echo date('m', strtotime($registro["dataEvento"])) ?></span>/<span id="anoEvento<?php echo $registro['ideventos']?>"><?php echo date('Y', strtotime($registro["dataEvento"])) ?></span>
                            <br>
                            Horário de inicio: <span id="horaEvento<?php echo $registro['ideventos']?>"><?php echo date('H', strtotime($registro["horaEvento"])) ?></span>h<?php echo date('i', strtotime($registro["horaEvento"])) ?>
                        </small>
                        <hr />
                        <!-- mini menu-->
                        <?php
                            $sql_inscricao = "SELECT * FROM inscricoes WHERE inscricoes.usuario_id = {$_POST['id']} AND inscricoes.evento_id = {$registro['ideventos']}";
                            $retorno = mysqli_query($link, $sql_inscricao);
                            $inscricoes = mysqli_num_rows($retorno);
                            if ($inscricoes == 1) {
                                $class = "fa-check-square";
                                $acao = "Desinscrever-se";
                            } else {
                                $class = "fa-check";
                                $acao = "Inscrever-se";
                            }
                        ?>
                        <div class="btn-toolbar float-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm tooltip-wrapper " data-tt="tooltip"
                                    data-placement="top" <?php echo "title='{$acao}'"?> onclick="acoes(<?php echo $registro['ideventos']?>, <?php echo $_POST['id']?>)" id="btn_inscrever-se<?php echo $registro['ideventos']?>">
                                    <i class="fa <?php echo $class?> toggleButton" id="modalButton<?php echo $registro['ideventos']?>"></i>
                                </button>
                                <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target=<?php echo "#evento" . $registro['ideventos'] . "Modal" ?>
                                    data-tt="tooltip" data-placement="top" title="Mais informações">
                                    <i class="fas fa-info"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal -->
            <div class="modal fade event" id=<?php echo "evento".$registro['ideventos']."Modal" ?> tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content" id=<?php echo "modal".$registro['ideventos']?>>
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"><?php echo $registro["nomeEvento"] ?></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <img class="card-img-top img-thumbnail" src=<?php echo "../upload/" . $registro['imagem'] ?> alt="">
                            <hr />
                            <p class="card-text"><?php echo $registro["descricao"] ?></p>
                            <small>Por <?php echo $registro["responsaveis"] ?></small><br>
                            <small>Para <?php echo $registro["publicoAlvo"] ?></small><br>
                            <small>Pré requisitos: <?php echo $registro["requisitos"] ?></small><br>
                            <small class="text-right">Local: <?php echo $registro["local"] ?></small><br>
                            <small class="vagas">Vagas ocupadas:
                            <span id="modal_qtdInscritos<?php echo $registro['ideventos']?>"><?php echo $numero_de_inscricoes ?></span>/<span id="qtdDisponivel<?php echo $registro['ideventos']?>"><?php echo $registro["quantidadeVagas"] ?></span>
                        </small><br>
                        <small class="data">Data do evento:
                            <span id="diaEvento<?php echo $registro['ideventos']?>"><?php echo date('d', strtotime($registro["dataEvento"])) ?></span>/<span id="mesEvento<?php echo $registro['ideventos']?>"><?php echo date('m', strtotime($registro["dataEvento"])) ?></span>/<span id="anoEvento<?php echo $registro['ideventos']?>"><?php echo date('Y', strtotime($registro["dataEvento"])) ?></span>
                            <br>
                            Horário de inicio: <span id="horaEvento<?php echo $registro['ideventos']?>"><?php echo date('H', strtotime($registro["horaEvento"])) ?></span>h<?php echo date('i', strtotime($registro["horaEvento"])) ?>
                        </small>
                            <hr />
                            <button class="btn btn-info btn-block" data-toggle="collapse" data-target="#alunosEvento">
                                <center>Alunos cadastrados no evento<span class="d-none d-sm-inline"> <i class="fas fa-caret-down"><span></i></center>
                            </button>
                            <div id="alunosEvento" class="collapse">
                                <br>
                                <ul class="list-group" id="lista_inscritos<?php echo $registro['ideventos']?>">
                                <?php
                                    $sql = "SELECT nome FROM usuarios, inscricoes, eventos 
                                    WHERE eventos.ideventos = " . $registro['ideventos'] . "
                                    AND inscricoes.usuario_id = usuarios.id
                                    AND inscricoes.evento_id = eventos.ideventos;";
                                    $r = mysqli_query($link, $sql);
                                    while($nomes = mysqli_fetch_assoc($r)){
                                        echo "<li>" . $nomes['nome'] . "</li>";
                                    }
                                ?>
                                </ul>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="btn-group btn-group-sm" role="group" aria-label="...">
                                <button type="button" class="btn btn-secondary" onclick="acoes(<?php echo $registro['ideventos']?>, <?php echo $_POST['id']?>)" id="btn_inscrever-se_modal_<?php echo $registro['ideventos']?>">
                                    <i class="fa <?php echo $class?> toggleButton" id="modalButton<?php echo $registro['ideventos']?>"></i>
                                    <span class="inscreva-se"><?php echo $acao?></span>
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                    <i class="far fa-window-close"></i> Fechar janela
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Fim bloco evento -->
            <?php
                if($registro['status'] == 0){
                    echo "<script>
                            desativar_evento({$registro['ideventos']});
                            var numero_eventos_ativos = +$('#numero_eventos_ativos').text();
                            numero_eventos_ativos--;
                            $('#numero_eventos_ativos').html(numero_eventos_ativos);
                        </script>";
                }

                    echo "  <script>
                        lotacao({$registro['ideventos']}); 
                        diaDeEventoBloquear({$registro['ideventos']});
                    </script>";   
                } 
            ?>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
    <!-- Footer -->
    <footer class="page-footer font-small bgColor">
        <!-- Footer Elements -->
        <div class="container text-white py-5">
            <div class="row center">
                <div class="col-3">
                </div>
                <div class="col-2">
                    <a href="https://www.facebook.com/engenhariascompsoft/"><i class="fab fa-facebook-f footerIcon"></i></a>
                </div>
                <div class="col-2">
                    <a href="https://www.instagram.com/engsoftwareunifae/"><i class="fab fa-instagram footerIcon"></i></a>
                </div>
                <div class="col-2 ">
                    <a href="https://api.whatsapp.com/send?phone=5519992183875&text=Olá%20Anderson!"><i class="fab fa-whatsapp footerIcon"></i></a>
                </div>
            </div>
        </div>
        <!-- Footer Elements -->
        <!-- Copyright -->
        <div class="footer-copyright text-center py-3 footerBgColor">
            © 2018 <b><span>UNI</span><span style="color: #FE2E2E">FAE</span> - Engenharia de Software</b>
        </div>
        <!-- Copyright -->
    </footer>
    <!-- Footer -->
    <script>
        $(document).ready(function(){
            $(".limit").each(function (i) {
                var text = $(this).text();
                var len = text.length;

                if (len > 80) {
                    var query = text.split(" ", 10);
                        query.push('...');
                        res = query.join(' ');
                    $(this).text(res);
                }
            });
            block_ao_logar(<?php echo $_POST['id']?>);
        });

        /* Tooltip bootstrap */
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
</body>

</html>